#include <stdarg.h>
#include <stdio.h>      /* vsnprintf */
#include <string>
#include <vector>
#include <map>
#include <list>
#include "Enclave.h"
#include "Enclave_t.h"  /* print_string */

std::map<int, std::list<int> > mapOfIndices;

/* 
 * printf: 
 *   Invokes OCALL to display the enclave buffer to the terminal.
 */
void printf(const char *fmt, ...)
{
    char buf[BUFSIZ] = {'\0'};
    va_list ap;
    va_start(ap, fmt);
    vsnprintf(buf, BUFSIZ, fmt, ap);
    va_end(ap);
    ocall_print_string(buf);
}

void send(std::string msg)
{
    char *msgchar = &msg[0];
    ocall_send(msgchar, msgchar, 5);
}

/*std::string receive()
{
    ocall_receive();
}*/

void printf_helloworld()
{
    printf("Hello World\n");
}


void loadGraph(const char *indices)
{
    printf("Inside of loadGraph!\n");
    //printf(indices);
    std::string input(indices);
    std::string line = "";
    std::map<int, std::list<int> > mapOfIndices;
    while (input != "")
    {
            printf("\n********\n");
            auto index_newline = input.find_first_of("\n");
            line = input.substr(0, index_newline);

            //get the user ID
            auto index_first_comma = line.find_first_of(",");
            int userId = std::stoi(line.substr(0, index_first_comma));
            std::string strId = std::to_string(userId);
            const char* c = strId.c_str();
            printf(c);
            printf("\n");
            //rest of the line after the ID
            std::string restLine = line.substr(index_first_comma+1, line.length());

            input = input.substr(index_newline+1, input.length());

            //get the movie indices that belong to userId
            std::string movieIndex = "";
            std::list<int> listOfIndices;
            while(restLine.compare(movieIndex) != 0)
            {
                    auto index_comma = restLine.find_first_of(",");
                    movieIndex = restLine.substr(0, index_comma);
                    listOfIndices.push_back(std::stoi(movieIndex));

                    restLine = restLine.substr(index_comma+1, restLine.length());
            }
            mapOfIndices[userId] = listOfIndices;

    }

}