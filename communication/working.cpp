#include <zmq.hpp>
#include <iostream>
#include <unistd.h>
#include <future>

class ZmqServer{
public:
    ZmqServer() : m_context(1), socket(m_context, ZMQ_ROUTER) {
    }

    void bind(std::string protocol ,int port){
        std::string bind_param;
        bind_param = protocol + "://*:" + std::to_string(port);
        socket.bind(bind_param);
    }

    void receive(){
        zmq::message_t request;
        //  Wait for next request from client
        socket.recv (&request);
        std::cout << "Received message : " << request << std::endl;
    }

    void send(std::string msg){
        //  Send reply back to client
        zmq::message_t reply (msg.size());
        memcpy (reply.data (), msg.c_str(), 5);
        socket.send (reply);
    }

private:
    zmq::context_t m_context;
    zmq::socket_t socket;
};

int main () {
    ZmqServer zmq = ZmqServer();
    zmq.bind("tcp", 5555);
    //auto sThread = std::thread( serverThread, std::ref(zmq));

    //sThread.join();

    return 0;
}
/*
#include <iostream>
#include <string>
#include "serv.h"

int main(){
        ZmqServer zmq = ZmqServer();
        zmq.bind("tcp", 5555);
        return 0;
}
*/