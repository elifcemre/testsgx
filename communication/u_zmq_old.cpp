#include <communication/u_zmq.h>
#include <limits.h>
#include <stringtools.h>
#include <unistd.h>
#include <iostream>

zmq::context_t *CommunicationZmq::context(nullptr);
//std::atomic<bool> CommunicationZmq::die(false);
//InputFunction CommunicationZmq::finput;
int CommunicationZmq::localport = 0;

//------------------------------------------------------------------------------
// Server
//------------------------------------------------------------------------------
static CommunicationZmq *communication = nullptr;
CommunicationZmq::CommunicationZmq(int port, bool dummy)
    : socket_(*context, zmq::socket_type::router) {}

//------------------------------------------------------------------------------
/*static bool CommunicationZmq::send(const std::string & string, int flags = 0){
    zmq::message_t message(string.size());
    memcpy (message.data(), string.data(), string.size());

    bool rc = communication->socket_.send (message, flags);
    return (rc);
}

//------------------------------------------------------------------------------
static bool CommunicationZmq::sendmore(const std::string & string){
    zmq::message_t message(string.size());
    memcpy (message.data(), string.data(), string.size());

    bool rc = communication->socket_.send (message, ZMQ_SNDMORE);
    return (rc);
}

//------------------------------------------------------------------------------
inline static std::string CommunicationZmq::recv (int flags = 0) {

    zmq::message_t message;
    communication->socket_.recv(&message, flags);

    return std::string(static_cast<char*>(message.data()), message.size());
}*/

//------------------------------------------------------------------------------
//std::set<std::pair<std::string, int>> CommunicationZmq::out_endpoints;
void CommunicationZmq::init(int port) {
    localport = port;

    context = new zmq::context_t(1);
    communication = new CommunicationZmq(port, true);

    communication->socket_.bind("tcp://*:"+std::to_string(port));
}

//------------------------------------------------------------------------------
ssize_t CommunicationZmq::send(const std::string &routing_id,
                               const void *buffer, size_t length) {
    if (communication == nullptr) {
        std::cerr << "Server not initialized" << std::endl;
        return 0;
    }

    std::vector<std::string> route = split(routing_id, " ");
    ssize_t ret = 0;
    for (auto hop = route.begin(); hop != route.end(); ++hop) {
        ret += communication->send_more(*hop, hop != route.begin());
        if (hop + 1 != route.end())
            ret += communication->send_more(std::string());
    }
    ret += communication->send(zmq::const_buffer(buffer, length));

    return ret;
}

