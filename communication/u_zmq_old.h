//#pragma once

//#include <communication_manager.h>

//#include <atomic>
#include <communication/zmq.hpp>
//#include <set>

class CommunicationZmq {
   public:
    CommunicationZmq(int p, bool dummy);        // server

    static bool send(const std::string & string, int flags = 0);
    static bool sendmore(const std::string & string);
    static std::string recv(int flags = 0);


    
    // If server: init, iterate, finish -------------------------------------------------------------
    static void init(int port);
    

   private:
    //static InputFunction finput;
    //static std::atomic<bool> die;
    static zmq::context_t* context;
    static int localport;
    zmq::socket_t socket_;
    //std::set<std::pair<std::string,int>> probed_;
};

//#include <communication/sync_zmq.hpp>
